#include <iostream>
#include <tuple>
#include <list>
#include <assert.h>
#include <unistd.h>
#include <temperaturesensor.h>

using namespace std;

namespace ThermalControl
{
	typedef int                                Speed;
	typedef int                                Temperature;
	typedef std::pair< Temperature, Speed  >   TemperatureSpeedPoint;
	typedef std::list< TemperatureSpeedPoint > TemperatureSpeedCurve;
	typedef TemperatureSpeedCurve::iterator    TemperatureSpeedCurveIterator;


	///////////////////////////////////////////////////////////////////////////
	/// @class   ThermalControlAlgorithm
	/// @brief   Interface for thermal control algorithms.
	/// @details Defines the interface used by ThermalController
	///////////////////////////////////////////////////////////////////////////
	struct ThermalControlAlgorithm
	{
		virtual ~ThermalControlAlgorithm() {}

		///////////////////////////////////////////////////////////////////////
		/// @param  A temperature reading.
		/// @return The calculated fan speed.
		///////////////////////////////////////////////////////////////////////
		virtual Speed calculateCollingPower( Temperature curTemp ) = 0;

	};


	///////////////////////////////////////////////////////////////////////////
	/// @class   DiscreteThermalControlAlgorithm
	/// @brief   Thermal control algorithm based on curves.
	///////////////////////////////////////////////////////////////////////////
	class DiscreteThermalControlAlgorithm : public ThermalControlAlgorithm
	{
	public:
		///////////////////////////////////////////////////////////////////////
		/// @brief Constructor
		/// @param upCurve    Curve used in the interpolation process when
		///                   temperature is raising.
		/// @param downCurve  Curve used in the interpolation process when
		///                   the temperature is decreasing.
		///////////////////////////////////////////////////////////////////////
		DiscreteThermalControlAlgorithm( TemperatureSpeedCurve upCurve, TemperatureSpeedCurve downCurve )
		: m_upCurve            ( upCurve  )
		, m_downCurve          ( downCurve)
		, m_currentSpeed       ( 0        )
		, m_previousTemperature( 0        )
		{

		}

		///////////////////////////////////////////////////////////////////////
		/// @brief   Linear interpolation of a x value based on two points of a curve.
		/// @details Follows an example of the same algorithm written in Haskell:
		/// @code
		///
		///		interpolate' :: Integral a
		///		            => a           -- ^ The x to interpolate
		///		            -> (a, a)      -- ^ The (xa, ya) point
		///		            -> (a, a)      -- ^ The (xb, yb) point
		///		            -> a           -- ^ The resulting y value
		///		interpolate' x (_, ya) (xb, yb) = yb - ((xb - x)*(yb - ya)) `div` (yb - ya)
		//
		/// @endcode
		///////////////////////////////////////////////////////////////////////
		Speed interpolate2Points( Temperature x, TemperatureSpeedPoint pointA, TemperatureSpeedPoint pointB )
		{
			const int xb = pointB.first;
			const int ya = pointA.second;
			const int yb = pointB.second;
			return  yb - ((xb - x)*(yb - ya)) / (yb - ya);
		}

		///////////////////////////////////////////////////////////////////////
		/// @brief   Linear interpolation of a x value on a curve.
		/// @details Follows an example of the same algorithm written in Haskell:
		/// @code
		///
		///		interpolate :: Temperature            -- ^ The x to interpolate
		///		            -> TemperatureSpeedCurve  -- ^ The curve to interpolate
		///		            -> Speed                  -- ^ The resulting Y
		///		interpolate _ []        = error "Interpolate needs at least one point"
		///		interpolate _ [(_, ya)] = ya
		///		interpolate x ( pointA@(xa, ya) : pointB@(xb, _) : xs )
		///		   | x <  xa            = ya
		///		   | x >= xa && x <= xb = interpolate' x  pointA pointB
		///		   | otherwise          = interpolate  x  ( pointB : xs )
		///
		/// @endcode
		///////////////////////////////////////////////////////////////////////
		Speed linearInterpolation( Temperature x, TemperatureSpeedCurve curve )
		{
			Speed finalSpeed = 0;
			assert( curve.size() );

			if ( curve.size() == 1 )
			{
				finalSpeed = curve.front().second;
			}
			else
			{
				TemperatureSpeedCurveIterator iterA = curve.begin();
				TemperatureSpeedCurveIterator iterB = iterA++;
				for ( ;
				      iterA != curve.end() && iterB != curve.end();
				      iterA++, iterB++ )
				{
					Temperature xa = (*iterA).first;
					Temperature xb = (*iterB).first;

					if ( x < xa )
					{
						finalSpeed = xa;
						break;
					}
					else if ( x > xa && x <= xb )
					{
						finalSpeed = interpolate2Points( x, *iterA, *iterB );
						break;
					}
				}
			}

			return finalSpeed;
		}

		///////////////////////////////////////////////////////////////////////
		///	@brief Calculate the resulting Fan speed based on a set of temperature vs speed curves
		/// @details Follows an example of the same algorithm written in Haskell:
		/// @code
		///
		///		discreteThermalAlgorithm :: DiscreteThermalAlgorithmData            -- ^ The context specific information required by the algorithm
		///		                         -> Temperature                             -- ^ The current temperature sampled
		///		                         -> (Speed , DiscreteThermalAlgorithmData)  -- ^ resulting fan speed plus context specific information
		///		discreteThermalAlgorithm (DiscreteThermalAlgorithmData ([], _) _ _ ) _ = error "Empty up temperature list"
		///		discreteThermalAlgorithm (DiscreteThermalAlgorithmData (_, []) _ _ ) _ = error "Empty down temperature list"
		///		discreteThermalAlgorithm (DiscreteThermalAlgorithmData curves@(upCurve, downCurve) curSpeed  lastTemp ) curTemp
		///		   | curTemp >= lastTemp && finalUpSpeed > curSpeed = ( finalUpSpeed, ( DiscreteThermalAlgorithmData curves finalUpSpeed curTemp ) )
		///		   | curTemp <  lastTemp && finalDwSpeed < curSpeed = ( finalDwSpeed, ( DiscreteThermalAlgorithmData curves finalUpSpeed curTemp ) )
		///		   | curSpeed == 0                                  = ( finalUpSpeed, ( DiscreteThermalAlgorithmData curves finalUpSpeed curTemp ) )
		///		   | otherwise                                      = ( curSpeed    , ( DiscreteThermalAlgorithmData curves finalUpSpeed curTemp ) )
		///		   where finalUpSpeed =  interpolate curTemp upCurve
		///		         finalDwSpeed =  interpolate curTemp downCurve
		///
		/// @endcode
		///////////////////////////////////////////////////////////////////////
		Speed calculateCollingPower( Temperature curTemp )
		{
			Speed finalSpeed = m_currentSpeed;
			assert( m_upCurve.size()   );
			assert( m_downCurve.size() );
			Speed finalUpSpeed = linearInterpolation(curTemp, m_upCurve );
			if ( curTemp >= m_previousTemperature && finalUpSpeed > m_currentSpeed )
			{
				finalSpeed = finalUpSpeed;
			}
			else
			{
				Speed finalDownSpeed = linearInterpolation( curTemp, m_downCurve );
				if ( curTemp < m_previousTemperature && finalDownSpeed < m_currentSpeed )
				{
					finalSpeed = finalDownSpeed;
				}
			}
			return finalSpeed;
		}

	private:

		TemperatureSpeedCurve m_upCurve;
		TemperatureSpeedCurve m_downCurve;
		Speed                 m_currentSpeed;
		Temperature           m_previousTemperature;

	};

	///////////////////////////////////////////////////////////////////////////
	/// @class   TemperatureSensor
	/// @brief   A C++ wrapper for the temperaturesensor lib functions.
	///////////////////////////////////////////////////////////////////////////
	class TemperatureSensorWrapper
	{
	public:
		///////////////////////////////////////////////////////////////////////
		/// @brief Constructor
		/// @param bus     A bus id
		/// @param address The address of the sensor
		///////////////////////////////////////////////////////////////////////
		TemperatureSensorWrapper( int bus, int address )
		: m_sensor ( TemperatureSensor_createI2cSensor( bus, address) )
		{

		}

		///////////////////////////////////////////////////////////////////////
		/// @brief Read a temperature.
		/// @return The read temperature.
		///////////////////////////////////////////////////////////////////////
		Temperature read()
		{
			return TemperatureSensor_read( m_sensor );
		}
	private:
		TemperatureSensor* m_sensor;
	};

	///////////////////////////////////////////////////////////////////////////
	/// @class   ThermalController
	/// @brief   Class responsible for the continuous execution of the thermal
	///          algorithms.
	///////////////////////////////////////////////////////////////////////////
	class ThermalController
	{
	public:
		typedef std::pair<TemperatureSensorWrapper*, ThermalControlAlgorithm*> SensorAlgorithmMap;
		typedef std::list< SensorAlgorithmMap >                         SensorsAndAlgorithms;
		typedef SensorsAndAlgorithms::iterator                          SensorsAndAlgorithmsIterator;

		///////////////////////////////////////////////////////////////////////
		/// @brief Constructor
		/// @param sensorsAndAlgorithms
		///////////////////////////////////////////////////////////////////////
		ThermalController( const SensorsAndAlgorithms& sensorsAndAlgorithms )
		: m_sensorAndAlgorithms( sensorsAndAlgorithms )
		{

		}

		////////////////////////////////////////////////////////////////////////
		/// @brief Method that never returns and continuously execute the
		///        thermal control.
		/// @details Follows an example of the same algorithm written in Haskell:
		/// @code
		///
		///		continuosThermalControl:: Show c => [a]                              -- ^ information relevant to the temperature reader function
		///		                                 -> (a -> IO Temperature)            -- ^ temperature reader function
		///		                                 -> (c -> Temperature -> (Speed, c)) -- ^ thermal algorithm, receives a context specific info, the temperature read a must return a speed and new context information
		///		                                 -> [c]                              -- ^ thermal algorithm context information
		///		                                 -> IO()
		///		continuosThermalControl sensors readTemp algorithm input = do
		///		   temperatures     <- mapM readTemp sensors
		///		   let output       =  zipWith algorithm input temperatures
		///		   let collingPower =  maximum $ finalCoolingPower output
		///		   printf "%s - %d\n" (show temperatures) collingPower
		///		   threadDelay 1000000
		///		   continuosThermalControl sensors readTemp algorithm (newInput output)
		///		   where newInput          = snd . unzip
		///		         finalCoolingPower = fst . unzip
		///
		/// @endcode
		////////////////////////////////////////////////////////////////////////
		void RunThermalControl( )
		{
			while( 1 )
			{
				Speed finalSpeed = 0;
				for ( SensorsAndAlgorithmsIterator iter = m_sensorAndAlgorithms.begin();
				      iter != m_sensorAndAlgorithms.end();
				      iter++ )
				{
					Temperature temperatureReading = (*iter).first->read();
					Speed       calculatedSpeed    = (*iter).second->calculateCollingPower( temperatureReading );
					if ( calculatedSpeed > finalSpeed )
					{
						finalSpeed = calculatedSpeed;
					}
				}
				cout << "Speed: " << finalSpeed << endl;
				usleep( 1000000 );
			}
		}

		SensorsAndAlgorithms m_sensorAndAlgorithms;
	};

}



int
main( int argc, char** argv )
{
	// Haskell curves declaration
	// let defaultCurves = ([ (10, 20), (14, 25), (20, 40), (50, 60), (75, 80), (90, 90) ],
	//                      [ ( 8, 20), (12, 25), (18, 40), (48, 60), (73, 80), (88, 90) ] )
	using namespace ThermalControl;
	TemperatureSpeedCurve defaultUpCurve;
	defaultUpCurve.push_back( make_pair( 10, 20));
	defaultUpCurve.push_back( make_pair( 14, 25));
	defaultUpCurve.push_back( make_pair( 20, 40));
	defaultUpCurve.push_back( make_pair( 50, 60));
	defaultUpCurve.push_back( make_pair( 75, 80));
	defaultUpCurve.push_back( make_pair( 90, 90));
	TemperatureSpeedCurve defaultDnCurve;
	defaultDnCurve.push_back( make_pair(  8, 20));
	defaultDnCurve.push_back( make_pair( 12, 25));
	defaultDnCurve.push_back( make_pair( 18, 40));
	defaultDnCurve.push_back( make_pair( 48, 60));
	defaultDnCurve.push_back( make_pair( 73, 80));
	defaultDnCurve.push_back( make_pair( 88, 90));
	TemperatureSensorWrapper* sensor1 = new TemperatureSensorWrapper( 0x00, 0xa0 );
	TemperatureSensorWrapper* sensor2 = new TemperatureSensorWrapper( 0x00, 0xa2 );
	TemperatureSensorWrapper* sensor3 = new TemperatureSensorWrapper( 0x00, 0xa4 );
	DiscreteThermalControlAlgorithm* sensor1Algo = new DiscreteThermalControlAlgorithm( defaultUpCurve, defaultDnCurve );
	DiscreteThermalControlAlgorithm* sensor2Algo = new DiscreteThermalControlAlgorithm( defaultUpCurve, defaultDnCurve );
	DiscreteThermalControlAlgorithm* sensor3Algo = new DiscreteThermalControlAlgorithm( defaultUpCurve, defaultDnCurve );
	ThermalController::SensorsAndAlgorithms sensorsAndAlgorithms;
	sensorsAndAlgorithms.push_back( make_pair(sensor1, sensor1Algo) );
	sensorsAndAlgorithms.push_back( make_pair(sensor2, sensor2Algo) );
	sensorsAndAlgorithms.push_back( make_pair(sensor3, sensor3Algo) );

	ThermalController controller( sensorsAndAlgorithms );
	controller.RunThermalControl();
}
