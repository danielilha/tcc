cmake_minimum_required(VERSION 2.8)

project(libtemperaturesensor)

set     ( CMAKE_C_FLAGS   "-g"   )
set     ( CMAKE_CXX_FLAGS "${CMAKE_C_FLAGS}" )

include_directories( include )

add_library( temperaturesensor STATIC src/temperaturesensor.c )

install(TARGETS temperaturesensor           DESTINATION lib    )
install(FILES   include/temperaturesensor.h DESTINATION include)
