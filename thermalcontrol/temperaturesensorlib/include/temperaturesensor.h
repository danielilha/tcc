#ifndef TEMPERATURE_SENSOR_H
#define TEMPERATURE_SENSOR_H

#include <stdio.h>

#if defined(__cplusplus)
extern "C"
{
#endif

typedef struct TemperatureSensor_ TemperatureSensor;

///////////////////////////////////////////////////////////////////////////////
/// @brief  Construct a I2C temperature sensor.
/// @param  bus      The I2C bus where the sensor is located.
/// @param  address  The address of the sensor on the bus.
/// @return A pointer to an allocated sensor object
///////////////////////////////////////////////////////////////////////////////
TemperatureSensor* TemperatureSensor_createI2cSensor( int bus, int address );
///////////////////////////////////////////////////////////////////////////////
/// @brief  Deletes a previous allocated temperature sensor.
/// @param  pSensor  A pointer to a previous allocated temperature sensor
/// @pre    pSensor != 0
///////////////////////////////////////////////////////////////////////////////
void               TemperatureSensor_delete         ( TemperatureSensor *pSensor );
///////////////////////////////////////////////////////////////////////////////
/// @brief  Reads a temperature sensor temperature.
/// @param  pSensor  A pointer to a previous allocated temperature sensor.
/// @pre    pSensor != 0
///////////////////////////////////////////////////////////////////////////////
int                TemperatureSensor_read           ( TemperatureSensor *pSensor );

#if defined(__cplusplus)
}
#endif

#endif // TEMPERATURE_SENSOR_H
