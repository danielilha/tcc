/*
 * port.h
 *
 *  Created on: May 16, 2014
 *      Author: daniel
 */

#ifndef PORT_H_
#define PORT_H_

#include <switchport.h>
#include <memory>
#include <list>

namespace switchmanagement
{
   class SasPort
   {
   public:
      enum EnSasLinkState
      {
         eNotReady = 0,
         eLinked,
         eDisabled
      };

      enum EnSasLinkSpeed
      {
         eSlow,
         eNormal,
         eFast
      };

      SasPort( int logical, int physical );

      ~SasPort();

      EnSasLinkSpeed getLinkSpeed() const;

      EnSasLinkState getLinkState() const;

      uint32 getLogicalPort() const;

      uint32 getPhysicalPort() const;

   private:
      SwitchPort* m_pPort;
   };

   typedef std::shared_ptr< SasPort > SasPortPtr;
   typedef std::list< SasPortPtr >    SasPorts;
   typedef SasPorts::iterator         SasPortsIter;

}

#endif /* PORT_H_ */
