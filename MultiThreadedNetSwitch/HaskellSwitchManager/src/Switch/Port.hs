-- |
-- Module      : $Header$
-- Description : Haskell bindings for switchport.h
-- Copyright   : (c) daniel.ilha@gmail.com
-- Maintainer  : daniel.ilha@gmail.com
-- Stability   : experimental
-- Portability : portable
-- 
{-# LANGUAGE ForeignFunctionInterface #-} 

module Switch.Port where

import Foreign.C -- get the C types
import Foreign

data SasLinkState = NotReady | Linked | Disabled deriving ( Eq, Show, Enum)
data SasLinkSpeed = Slow | Normal | Fast         deriving ( Eq, Show, Enum)

-- |
-- Connection type
--
data SwitchPort       = SwitchPort
-- |
-- Connection pointer type
--       
type SwitchPortPtr    = Ptr SwitchPort
-- |
-- Connection handle type. This holds a ForeignPtr instance
--
data SwitchPortHandle = SwitchPortHandle (ForeignPtr SwitchPort) 


-- |
-- Create a switch port
-- 
createSwitchPort :: (Int, Int) -> IO SwitchPortHandle 
createSwitchPort (logical, physical) = do
   port <- c_SwitchPortCreateSwitchPort (fromIntegral logical) (fromIntegral physical)
   portPtr <- newForeignPtr c_SwitchPortDelete port
   return (SwitchPortHandle  portPtr )

-- |
-- Get the port link speed
--
getLinkSpeed :: SwitchPortHandle -> IO SasLinkSpeed
getLinkSpeed (SwitchPortHandle port) = 
   withForeignPtr  port $ \portPtr -> do 
      s <- c_SwithPortGetLinkSpeed portPtr
      return $ toEnum (fromIntegral s)

-- |
-- Get the port link state
--
getLinkState :: SwitchPortHandle -> IO SasLinkState
getLinkState (SwitchPortHandle port) = 
   withForeignPtr  port $ \portPtr -> do 
      s <- c_SwithPortGetLinkState portPtr
      return $ toEnum (fromIntegral s)

-- |
-- Get the port link state
--
getLogicalPort :: SwitchPortHandle -> IO Int
getLogicalPort (SwitchPortHandle port) = 
   withForeignPtr  port $ \portPtr -> do 
      s <- c_SwithPortGetLogicalPort portPtr
      return $ fromIntegral s
      
      
-- |
-- Get the port link state
--
getPhysicalPort :: SwitchPortHandle -> IO Int
getPhysicalPort (SwitchPortHandle port) = 
   withForeignPtr  port $ \portPtr -> do 
      s <- c_SwithPortGetPhysicalPort portPtr
      return $ fromIntegral s

-- |
-- C functions imported by this module. These are not exported by this module.
-- 
foreign import ccall "switchportlib SwitchPort_createSwitchPort"    c_SwitchPortCreateSwitchPort   :: CInt -> CInt -> IO SwitchPortPtr
foreign import ccall "switchportlib &SwitchPort_delete"             c_SwitchPortDelete             :: FinalizerPtr SwitchPort
foreign import ccall "switchportlib SwitchPort_getLinkSpeed"        c_SwithPortGetLinkSpeed        :: SwitchPortPtr -> IO CInt
foreign import ccall "switchportlib SwitchPort_getLinkState"        c_SwithPortGetLinkState        :: SwitchPortPtr -> IO CInt
foreign import ccall "switchportlib SwitchPort_getLogicalPort"      c_SwithPortGetLogicalPort      :: SwitchPortPtr -> IO CInt
foreign import ccall "switchportlib SwitchPort_getPhysicalPort"     c_SwithPortGetPhysicalPort     :: SwitchPortPtr -> IO CInt

 