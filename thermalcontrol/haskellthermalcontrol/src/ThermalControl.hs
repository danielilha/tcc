-- Module      : $Header$
-- Description : This module contains generic implementation of a thermal control
--               algorigthm based on thermal curves.
-- Copyright   : (c) daniel.ilha@gmail.com
-- Maintainer  : daniel.ilha@gmail.com
-- Stability   : experimental
-- Portability : portable
-- 
-- Write a description of the algorithm here.
module ThermalControl(continuosThermalControl, discreteThermalAlgorithm, Speed, Temperature, TemperatureSpeedCurve, DiscreteThermalAlgorithmData(..)) where

import Control.Concurrent
import Text.Printf

type Speed                 = Int
type Temperature           = Int
type TemperatureSpeedPoint = (Temperature, Speed)
type TemperatureSpeedCurve = [ TemperatureSpeedPoint ] 

-- 
-- Keep a continuos calculation of the cooling power necessary to cool down the system
-- 
continuosThermalControl:: Show c => [a]                              -- ^ information relevant to the temperature reader function
                                 -> (a -> IO Temperature)            -- ^ temperature reader function
                                 -> (c -> Temperature -> (Speed, c)) -- ^ thermal algorithm, receives a context specific info, the temperature read a must return a speed and new context information
                                 -> [c]                              -- ^ thermal algorithm context information 
                                 -> IO()
continuosThermalControl sensors readTemp algorithm input = do
   temperatures     <- mapM readTemp sensors
   let output       =  zipWith algorithm input temperatures
   let collingPower =  maximum $ finalCoolingPower output
   _ <- printf "%s - %d\n" (show temperatures) collingPower
   threadDelay 1000000
   continuosThermalControl sensors readTemp algorithm (newInput output)
   where newInput          = snd . unzip 
         finalCoolingPower = fst . unzip


--
-- Linear interpolation of a x value based on two points of a curve.
-- 
interpolate' :: Integral a
            => a           -- ^ The x to interpolate
            -> (a, a)      -- ^ The (xa, ya) point
            -> (a, a)      -- ^ The (xb, yb) point
            -> a           -- ^ The resulting y value
interpolate' x (_, ya) (xb, yb) = yb - ((xb - x)*(yb - ya)) `div` (yb - ya)

-- 
-- Linear interpolation of a x value on a curve.
-- 
interpolate :: Temperature            -- ^ The x to interpolate
            -> TemperatureSpeedCurve  -- ^ The curve to interpolate
            -> Speed                  -- ^ The resulting Y
interpolate _ []        = error "Interpolate needs at least one point"
interpolate _ [(_, ya)] = ya
interpolate x ( pointA@(xa, ya) : pointB@(xb, _) : xs )
   | x <  xa            = ya
   | x >= xa && x <= xb = interpolate' x  pointA pointB
   | otherwise          = interpolate  x  ( pointB : xs )


data DiscreteThermalAlgorithmData = DiscreteThermalAlgorithmData { tempVsSpeedCurves   :: ( TemperatureSpeedCurve, TemperatureSpeedCurve )
                                                                 , currentCoolingPower :: Speed
                                                                 , previousTemperature :: Temperature } deriving (Show)


-- 
-- Calculate the resulting Fan speed based on a set of temperature vs speed curves
-- 
discreteThermalAlgorithm :: DiscreteThermalAlgorithmData            -- ^ The context specific information required by the algorithm
                         -> Temperature                             -- ^ The current temperature sampled
                         -> (Speed , DiscreteThermalAlgorithmData)  -- ^ resulting fan speed plus context specific information
discreteThermalAlgorithm (DiscreteThermalAlgorithmData ([], _) _ _ ) _ = error "Empty up temperature list"
discreteThermalAlgorithm (DiscreteThermalAlgorithmData (_, []) _ _ ) _ = error "Empty down temperature list"
discreteThermalAlgorithm (DiscreteThermalAlgorithmData curves@(upCurve, downCurve) curSpeed  lastTemp ) curTemp
   | curTemp >= lastTemp && finalUpSpeed > curSpeed = ( finalUpSpeed, ( DiscreteThermalAlgorithmData curves finalUpSpeed curTemp ) )
   | curTemp <  lastTemp && finalDwSpeed < curSpeed = ( finalDwSpeed, ( DiscreteThermalAlgorithmData curves finalDwSpeed curTemp ) )
   | curSpeed == 0                                  = ( finalUpSpeed, ( DiscreteThermalAlgorithmData curves finalUpSpeed curTemp ) )
   | otherwise                                      = ( curSpeed    , ( DiscreteThermalAlgorithmData curves curSpeed     curTemp ) )
   where finalUpSpeed =  interpolate curTemp upCurve
         finalDwSpeed =  interpolate curTemp downCurve
         


                                
