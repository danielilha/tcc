#include "switchport.h"
#include <sys/socket.h>
#include <netdb.h>
#include <stdlib.h>

typedef struct SwitchPort_
{
   uint32 m_logicalPort;
   uint32 m_physicalPort;
   uint32 m_linkState;
   uint32 m_speed;
} SwitchPort_;

///////////////////////////////////////////////////////////////////////////////
/// @brief  Construct a SasSwitchPort object.
/// @param  logicalPort   The logical port number.
/// @param  physicalPort  The physical port number.
/// @return A pointer to an allocated SwitchPort object
///////////////////////////////////////////////////////////////////////////////
SwitchPort*           SwitchPort_createSwitchPort( int logicalPort,
                                                   int physicalPort )
{
   SwitchPort* pPort = calloc( 1, sizeof(SwitchPort_) );
   pPort->m_logicalPort  = logicalPort;
   pPort->m_physicalPort = physicalPort;
   pPort->m_linkState    = ePORT_LINK_STATE_NOT_READY;
   pPort->m_speed        = ePORT_LINK_SPEED_NORMAL;
   return pPort;
}
///////////////////////////////////////////////////////////////////////////////
/// @brief  Deletes a previous allocated SwitchPort object.
/// @param  pPort  A pointer to a previous allocated SwitchPort object.
/// @pre    pPort != 0
///////////////////////////////////////////////////////////////////////////////
void                  SwitchPort_delete( SwitchPort *pPort )
{
   free( pPort );
}


///////////////////////////////////////////////////////////////////////////////
/// @brief Getters
/// @{
///////////////////////////////////////////////////////////////////////////////
int                   SwitchPort_getLogicalPort ( const SwitchPort* pPort )
{
   return pPort->m_logicalPort;
}
int                   SwitchPort_getPhysicalPort( const SwitchPort* pPort )
{
   return pPort->m_physicalPort;
}
EnSasLinkSpeed        SwitchPort_getLinkSpeed( const SwitchPort* pPort )
{
   return pPort->m_speed;
}
EnSasLinkState        SwitchPort_getLinkState( const SwitchPort* pPort )
{
   return pPort->m_linkState;
}
///////////////////////////////////////////////////////////////////////////////
/// @}
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
/// @brief Setters
/// @{
///////////////////////////////////////////////////////////////////////////////
void                  SwitchPort_setLinkSpeed( SwitchPort* pPort, EnSasLinkSpeed speed )
{
   pPort->m_speed = speed;
}
void                  SwitchPort_enable      ( SwitchPort* pPort )
{
   pPort->m_linkState = ePORT_LINK_STATE_NOT_READY;
}
void                  SwitchPort_disable     ( SwitchPort* pPort )
{
   pPort->m_linkState = ePORT_LINK_STATE_DISABLED;
}
///////////////////////////////////////////////////////////////////////////////
/// @}
///////////////////////////////////////////////////////////////////////////////

