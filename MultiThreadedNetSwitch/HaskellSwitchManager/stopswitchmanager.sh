#!/bin/bash

killall -2 HaskellSwitchManager > /dev/null 2>&1
while [[ $? == 0 ]]
do
	echo .
	killall -2 HaskellSwitchManager > /dev/null 2>&1
done

echo HaskellSwitchManager stopped
