/*
 * endianess.h
 *
 *  Created on: May 16, 2014
 *      Author: daniel
 */

#ifndef ENDIANESS_H_
#define ENDIANESS_H_

#include <switchporttypes.h>

/*
** Macro Definitions
*/
#define GSEP_BYTE_SWAP_16(x)                         \
        (uint16)(((((uint16)(x)) >> 8) & 0xFF)   | \
                  ((((uint16)(x)) << 8) & 0xFF00))

#define GSEP_BYTE_SWAP_24(x)                              \
        (uint32)( ((((uint32)(x)) >> 16) & 0x0000FF)  | \
                   ((((uint32)(x))      ) & 0x00FF00)  | \
                   ((((uint32)(x)) << 16) & 0xFF0000) )


#define GSEP_BYTE_SWAP_32(x)                            \
        (uint32)(((((uint32)(x)) >> 24) & 0xFF)     | \
                 ((((uint32)(x)) >>  8) & 0xFF00)    | \
                 ((((uint32)(x)) <<  8) & 0xFF0000)  | \
                 ((((uint32)(x)) << 24) & 0xFF000000))

#define GSEP_BYTE_SWAP_64(x) \
        (uint64)(((((uint64)(x)) >> 56) & 0x00000000000000FF) | ((((uint64)(x)) >> 40) & 0x000000000000FF00) | \
                  ((((uint64)(x)) >> 24) & 0x0000000000FF0000) | ((((uint64)(x)) >>  8) & 0x00000000FF000000) | \
                  ((((uint64)(x)) <<  8) & 0x000000FF00000000) | ((((uint64)(x)) << 24) & 0x0000FF0000000000) | \
                  ((((uint64)(x)) << 40) & 0x00FF000000000000) | ((((uint64)(x)) << 56) & 0xFF00000000000000))

#if !defined(CPU_LITTLE)
#define GSEP_BIG_ENDIAN_16(x) (x)
#define GSEP_BIG_ENDIAN_24(x) (x)
#define GSEP_BIG_ENDIAN_32(x) (x)
#define GSEP_BIG_ENDIAN_64(x) (x)
#else
#define GSEP_BIG_ENDIAN_16(x) GSEP_BYTE_SWAP_16(x)
#define GSEP_BIG_ENDIAN_24(x) GSEP_BYTE_SWAP_24(x)
#define GSEP_BIG_ENDIAN_32(x) GSEP_BYTE_SWAP_32(x)
#define GSEP_BIG_ENDIAN_64(x) GSEP_BYTE_SWAP_64(x)
#endif


#if defined(CPU_LITTLE)

   /////////////////////////////////////////////////////////////////////////////
   /// @brief Use this macro when declaring bit fields in a structure that
   ///        should be transmitted in LITTLE_ENDIAN
   /// @param fX  Field name
   /// @param sX  Size in bits of field X
   /////////////////////////////////////////////////////////////////////////////

   #define DECLARE_1_BITFIELDS( f0, s0 ) \
      uint32 f0:s0;

   #define DECLARE_2_BITFIELDS( f0, s0, f1, s1 ) \
      uint32 f1:s1; \
      DECLARE_1_BITFIELDS( f0, s0 )

   #define DECLARE_3_BITFIELDS( f0, s0, f1, s1, f2, s2 ) \
      uint32 f2:s2; \
      DECLARE_2_BITFIELDS( f0, s0, f1, s1)

   #define DECLARE_4_BITFIELDS( f0, s0, f1, s1, f2, s2, f3, s3 ) \
      uint32 f3:s3; \
      DECLARE_3_BITFIELDS( f0, s0, f1, s1, f2, s2 )

   #define DECLARE_5_BITFIELDS( f0, s0, f1, s1, f2, s2, f3, s3, f4, s4 ) \
      uint32 f4:s4; \
      DECLARE_4_BITFIELDS( f0, s0, f1, s1, f2, s2, f3, s3 )

   #define DECLARE_6_BITFIELDS( f0, s0, f1, s1, f2, s2, f3, s3, f4, s4, f5, s5 ) \
      uint32 f5:s5; \
      DECLARE_5_BITFIELDS( f0, s0, f1, s1, f2, s2, f3, s3, f4, s4 )

   #define DECLARE_7_BITFIELDS( f0, s0, f1, s1, f2, s2, f3, s3, f4, s4, f5, s5, f6, s6 ) \
      uint32 f6:s6; \
      DECLARE_6_BITFIELDS( f0, s0, f1, s1, f2, s2, f3, s3, f4, s4, f5, s5 )

   #define DECLARE_8_BITFIELDS( f0, s0, f1, s1, f2, s2, f3, s3, f4, s4, f5, s5, f6, s6, f7, s7 ) \
      uint32 f7:s7; \
      DECLARE_7_BITFIELDS( f0, s0, f1, s1, f2, s2, f3, s3, f4, s4, f5, s5, f6, s6 )

#else

   /////////////////////////////////////////////////////////////////////////////
   /// @brief Use this macro when declaring bit fields in a structure that
   ///        should be transmitted in BIG_ENDIAN
   /// @param fX  Field name
   /// @param sX  Size in bits of field X
   /////////////////////////////////////////////////////////////////////////////

   #define DECLARE_1_BITFIELDS( f0, s0 ) \
      uint32 f0:s0;

   #define DECLARE_2_BITFIELDS( f0, s0, f1, s1 ) \
      DECLARE_1_BITFIELDS( f0, s0 ) \
      uint32 f1:s1;

   #define DECLARE_3_BITFIELDS( f0, s0, f1, s1, f2, s2 ) \
      DECLARE_2_BITFIELDS( f0, s0, f1, s1) \
      uint32 f2:s2;

   #define DECLARE_4_BITFIELDS( f0, s0, f1, s1, f2, s2, f3, s3 ) \
      DECLARE_3_BITFIELDS( f0, s0, f1, s1, f2, s2 ) \
      uint32 f3:s3;

   #define DECLARE_5_BITFIELDS( f0, s0, f1, s1, f2, s2, f3, s3, f4, s4 ) \
      DECLARE_4_BITFIELDS( f0, s0, f1, s1, f2, s2, f3, s3 ) \
      uint32 f4:s4;

   #define DECLARE_6_BITFIELDS( f0, s0, f1, s1, f2, s2, f3, s3, f4, s4, f5, s5 ) \
      DECLARE_5_BITFIELDS( f0, s0, f1, s1, f2, s2, f3, s3, f4, s4 ) \
      uint32 f5:s5;

   #define DECLARE_7_BITFIELDS( f0, s0, f1, s1, f2, s2, f3, s3, f4, s4, f5, s5, f6, s6 ) \
      DECLARE_6_BITFIELDS( f0, s0, f1, s1, f2, s2, f3, s3, f4, s4, f5, s5 ) \
      uint32 f6:s6;

   #define DECLARE_8_BITFIELDS( f0, s0, f1, s1, f2, s2, f3, s3, f4, s4, f5, s5, f6, s6, f7, s7 ) \
      DECLARE_7_BITFIELDS( f0, s0, f1, s1, f2, s2, f3, s3, f4, s4, f5, s5, f6, s6 ) \
      uint32 f7:s7; \

#endif







#endif /* ENDIANESS_H_ */
