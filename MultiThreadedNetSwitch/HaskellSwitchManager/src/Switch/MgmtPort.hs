-- |
-- Module      : $Header$
-- Description : Haskell bindings for switchmgmtport.h
-- Copyright   : (c) daniel.ilha@gmail.com
-- Maintainer  : daniel.ilha@gmail.com
-- Stability   : experimental
-- Portability : portable
-- 
{-# LANGUAGE ForeignFunctionInterface #-} 
{-# LANGUAGE DeriveDataTypeable #-}
module Switch.MgmtPort ( SwitchMgmtConnectionHandle(..), SwitchMgmtExcpetion(..), openConnection, listen, handleRequest )  where

import Foreign.C -- get the C types
import Foreign.Marshal.Alloc()
import Foreign
import qualified Data.ByteString      as B
import Data.ByteString.Unsafe
import Data.Dynamic
import Control.Exception
import System.Mem
-- |
-- Connection type
--
data SwitchMgmtConnection       = SwitchMgmtConnection
-- |
-- Connection pointer type
--       
type SwitchMgmtConnectionPtr    = Ptr SwitchMgmtConnection
-- |
-- Connection handle type. This holds a ForeignPtr instance
--
data SwitchMgmtConnectionHandle = SwitchMgmtConnectionHandle (ForeignPtr SwitchMgmtConnection)

-- |
-- Exception type for errors that occur in the Switch.MgmtPort
--
data SwitchMgmtExcpetion = ConnectionError
   deriving (Show, Typeable )
   
instance Exception SwitchMgmtExcpetion


-- | 
-- Function responsible for opening a management port connection.
openConnection :: Word16 -> Word32 -> IO SwitchMgmtConnectionHandle
openConnection port maxConnection = newConnectionHandle $ c_SwitchMgmtPortOpenConnection (fromIntegral port) (fromIntegral maxConnection)

-- |   
-- Listen to a management port connection. It blocks until
-- there's a connection.
-- This is the C function signature:
-- SwitchMgmtConnection* SwitchMgmtPort_listen( SwitchMgmtConnection* pListeningConnection );
-- 
listen :: SwitchMgmtConnectionHandle -> IO SwitchMgmtConnectionHandle
listen (SwitchMgmtConnectionHandle listeningConnection) = 
   withForeignPtr  listeningConnection $ \listeningConnectionPtr -> newConnectionHandle $ c_SwitchMgmtPortListen listeningConnectionPtr

-- |
-- A more functional way of handling requests. This Haskell binding over the
-- C apis makes it very natural to Haskell API clients. The complexity of
-- managing memory allocations, necessary when using FFI, are completely 
-- abstracted.   
--
handleRequest :: SwitchMgmtConnectionHandle -> ( B.ByteString -> IO B.ByteString) -> IO ()  
handleRequest (SwitchMgmtConnectionHandle connection) f = do
   let buffer = B.replicate 1024 0
   withForeignPtr  connection $ \connectionPtr -> 
      unsafeUseAsCString buffer $ \cBuffer -> do
         rcvdBytes     <- c_SwitchMgmtPortReceive connectionPtr cBuffer 1024
         txBuffer  <- f (B.take rcvdBytes buffer)
         unsafeUseAsCString txBuffer $ \cbuff -> do
            c_SwitchMgmtPortSend connectionPtr (castPtr cbuff) (fromIntegral (B.length txBuffer) )
            -- performGC
            c_SwitchMgmtPortCloseConnection connectionPtr
            return ( )

-- |
-- Verify if the connection and throw and exception upon errors.
-- 
handleError :: SwitchMgmtConnectionPtr -> IO ()
handleError connection  
   | 0 > c_SwitchMgmtPortGetError connection = throw ConnectionError
   | otherwise = return ()
   
-- |
-- Create a new ConnectionHandle if the connection is fine. 
newConnectionHandle :: IO SwitchMgmtConnectionPtr -> IO SwitchMgmtConnectionHandle
newConnectionHandle con = do
   connection <- con
   connectionPtr <- newForeignPtr c_SwitchMgmtPortDeleteConnection connection
   handleError connection
   return (SwitchMgmtConnectionHandle  connectionPtr )

-- |
-- C functions imported by this module. These are not exported by this module.
-- 
foreign import ccall "switchportlib SwitchMgmtPort_OpenConnection"    c_SwitchMgmtPortOpenConnection   :: CUShort -> CUInt -> IO SwitchMgmtConnectionPtr
foreign import ccall "switchportlib SwitchMgmtPort_listen"            c_SwitchMgmtPortListen           :: SwitchMgmtConnectionPtr -> IO SwitchMgmtConnectionPtr
foreign import ccall "switchportlib SwitchMgmtPort_receive"           c_SwitchMgmtPortReceive          :: SwitchMgmtConnectionPtr -> CString -> CInt -> IO Int
foreign import ccall "switchportlib SwitchMgmtPort_send"              c_SwitchMgmtPortSend             :: SwitchMgmtConnectionPtr -> CString -> CInt -> IO Int
foreign import ccall "switchportlib SwitchMgmtPort_getError"          c_SwitchMgmtPortGetError         :: SwitchMgmtConnectionPtr -> CInt
foreign import ccall "switchportlib SwitchMgmtPort_closeConnection"   c_SwitchMgmtPortCloseConnection  :: SwitchMgmtConnectionPtr -> IO ()
foreign import ccall "switchportlib &SwitchMgmtPort_deleteConnection" c_SwitchMgmtPortDeleteConnection :: FinalizerPtr SwitchMgmtConnection

