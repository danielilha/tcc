module Main where

import Control.Monad
import Control.Concurrent
import qualified Switch.MgmtPort as MgmtPort
import qualified Switch.Smp      as Smp
import qualified Switch.Port     as Port
import Control.Exception
import qualified Data.ByteString as B

-- |
-- A loopback handler. Just sends back what is received. This 
-- is very handy for testing the conectivity.
-- 
loopbackHandler :: B.ByteString -> IO B.ByteString
loopbackHandler = return

-- |
-- Listen to connections and fork process for each opened one
--
listenForConnections:: MgmtPort.SwitchMgmtConnectionHandle -> [Port.SwitchPortHandle] -> IO ()
listenForConnections listeningConnection ports = do
   connection <- MgmtPort.listen listeningConnection
   forkIO $ MgmtPort.handleRequest connection ( Smp.handleManagementProtocol ports )
   return ()
     
-- |
-- Main fuction. Just start listening to connections
-- 
main :: IO ()
main = do 
   let logicalToPhysical = [ (x, x + 10) | x <- [0..32] ]
   ports <- mapM Port.createSwitchPort logicalToPhysical
    
   listeningConnection <- MgmtPort.openConnection 5002 10
   forever $ 
      catch ( listenForConnections listeningConnection ports ) 
            (\e -> do 
               print ( e :: MgmtPort.SwitchMgmtExcpetion )
               return ())   
