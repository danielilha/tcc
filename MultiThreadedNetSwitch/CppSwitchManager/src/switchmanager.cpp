#include <stdlib.h>
#include <iostream>
#include <switchporttypes.h>
#include <switchmgmtport.h>
#include <thread>
#include "smp.h"
#include "port.h"

using namespace std;

void Loopback( SwitchMgmtConnection* pConnection )
{
   uint8 pBuffer[4 * 1024];
   uint32 len = SwitchMgmtPort_receive( pConnection, pBuffer, sizeof( pBuffer ) );
   SwitchMgmtPort_send( pConnection, pBuffer, len );
   SwitchMgmtPort_closeConnection( pConnection );
   SwitchMgmtPort_deleteConnection( pConnection );
}


void SmpHandle( SwitchMgmtConnection* pConnection )
{
   uint8 pBuffer[4 * 1024];
   uint32 len = SwitchMgmtPort_receive( pConnection, pBuffer, sizeof( pBuffer ) );
   uint8* pResponse = smp::SmpController::getInstance().processSmpRequest( pBuffer, len );
   SwitchMgmtPort_send( pConnection, pResponse, len );
   SwitchMgmtPort_closeConnection( pConnection );
   SwitchMgmtPort_deleteConnection( pConnection );
   delete[] (pResponse);
}

int
main( int argc, char** argv )
{
   uint8 pBuffer[4 * 1024];
   switchmanagement::SasPorts ports;
   for ( int i = 0; i <= 32; i++ )
   {
      switchmanagement::SasPortPtr pPort = make_shared< switchmanagement::SasPort >( i, i + 10);
      ports.push_back( pPort );
   }

   smp::SmpController::getInstance().AddHandler( smp::eSmpFuntionDiscoverList,
                                                 make_shared< smp::SmpDiscoverListCommandHandler >( ports ));
   SwitchMgmtConnection* pMgmtListeningPort = SwitchMgmtPort_OpenConnection( 50001, 10 );

   while ( SwitchMgmtConnection* pConnection = SwitchMgmtPort_listen( pMgmtListeningPort ) )
   {
      thread ( SmpHandle, pConnection ).detach();
   }
   SwitchMgmtPort_closeConnection( pMgmtListeningPort );
   SwitchMgmtPort_deleteConnection( pMgmtListeningPort );

}
