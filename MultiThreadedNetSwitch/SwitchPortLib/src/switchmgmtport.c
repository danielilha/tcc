#include "switchmgmtport.h"
#include <sys/socket.h>
#include <netdb.h>
#include <stdlib.h>
#include <time.h>
#include <limits.h>

#define NANOS 1000000000LL

static unsigned long long sm_listeningTimeAcumulated = 0;
static unsigned long long sm_rxTxTimeAcumulated      = 0;
static unsigned long long sm_maxListeningTime        = 0;
static unsigned long long sm_maxRxTxTime             = 0;
static unsigned long long sm_numConnections          = 0;
static unsigned long long sm_numRx                   = 0;

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
typedef struct SwitchMgmtConnection_
{
	int                m_fd;
	int                m_initialConnection;
	struct sockaddr_in m_serverAddress;
	struct timespec    m_listeningTime;
	struct timespec    m_rxTime;
} SwitchMgmtConnection_;

void sampleConnectionTime( SwitchMgmtConnection* pConnection)
{
   clock_gettime( CLOCK_REALTIME , &pConnection->m_listeningTime );
}

void sampleReceptionTime( SwitchMgmtConnection* pConnection)
{
   clock_gettime( CLOCK_REALTIME , &pConnection->m_rxTime );
}

void calculateConnectionTime( SwitchMgmtConnection* pConnection)
{
   if ( pConnection->m_initialConnection == 0 )
   {
      struct timespec end;
      if (!clock_gettime( CLOCK_REALTIME , &end ) )
      {
         unsigned long long listeningTime = ( end.tv_sec  - pConnection->m_listeningTime.tv_sec) * NANOS +
                                            ( end.tv_nsec - pConnection->m_listeningTime.tv_nsec);
         if ( sm_listeningTimeAcumulated + listeningTime < LLONG_MAX)
         {
            sm_listeningTimeAcumulated += listeningTime;
            sm_numConnections++;
            if ( sm_maxListeningTime < listeningTime )
            {
               sm_maxListeningTime = listeningTime;
            }
         }

      }

   }
}


void calculateReceptionTime( SwitchMgmtConnection* pConnection)
{
   struct timespec end;
   if (!clock_gettime( CLOCK_REALTIME , &end ) )
   {
      unsigned long long listeningTime = ( end.tv_sec  - pConnection->m_rxTime.tv_sec) * NANOS +
                                         ( end.tv_nsec - pConnection->m_rxTime.tv_nsec);
      if ( sm_rxTxTimeAcumulated + listeningTime < LLONG_MAX )
      {
         sm_rxTxTimeAcumulated += listeningTime;
         sm_numRx++;
         if ( sm_maxRxTxTime < listeningTime )
         {
            sm_maxRxTxTime = listeningTime;
         }
      }

   }
}

void dumpTimeStats()
{
   printf("Execution times:\n"
         "  - Average connection time: %llu ns %llu us\n"
         "  - Average reception time : %llu ns %llu us\n"
         "  - Max connection time    : %llu ns %llu us\n"
         "  - Max reception time     : %llu ns %llu us\n"
         , sm_listeningTimeAcumulated / sm_numConnections, (sm_listeningTimeAcumulated / sm_numConnections)/ 1000
         , sm_rxTxTimeAcumulated      / sm_numConnections, (sm_rxTxTimeAcumulated      / sm_numRx) / 1000
         , sm_maxListeningTime                           , sm_maxListeningTime / 1000
         , sm_maxRxTxTime                                , sm_maxRxTxTime      / 1000 );
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
SwitchMgmtConnection*
SwitchMgmtPort_OpenConnection( uint16 port,
    						          uint32 maxConnections )
{
	SwitchMgmtConnection* pListeningConnection = calloc( 1, sizeof(SwitchMgmtConnection_) );
	pListeningConnection->m_fd = socket( AF_INET, SOCK_STREAM, 0);
	printf( "Socket %d\n", pListeningConnection->m_fd );

	pListeningConnection->m_serverAddress.sin_family      = AF_INET;
	pListeningConnection->m_serverAddress.sin_addr.s_addr = htonl( INADDR_ANY );
	pListeningConnection->m_serverAddress.sin_port        = htons( port );
	pListeningConnection->m_initialConnection = 1;
	// set SO_REUSEADDR on a socket to true (1):
	int optval = 1;
	setsockopt(pListeningConnection->m_fd, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof optval);

	bind( pListeningConnection->m_fd, (struct sockaddr*)&pListeningConnection->m_serverAddress, sizeof(pListeningConnection->m_serverAddress) );
	int listenResult = listen( pListeningConnection->m_fd, maxConnections );

	if ( -1 ==  listenResult )
	{
		printf( "Unable to listed %d", listenResult );
	}

	return pListeningConnection;
}


///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
void
SwitchMgmtPort_closeConnection( SwitchMgmtConnection* pConnection)
{
	close ( pConnection->m_fd );
	pConnection->m_fd = -1;
	calculateConnectionTime( pConnection );
	printf("SwitchMgmtPort close\n" );
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
void SwitchMgmtPort_deleteConnection( SwitchMgmtConnection* pConnection)
{
   printf("SwitchMgmtPort delete\n" );
   dumpTimeStats();

   if ( pConnection->m_fd >= 0 ) close ( pConnection->m_fd );
   free  ( pConnection );
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
int32                SwitchMgmtPort_getError( SwitchMgmtConnection* pConnection )
{
   int32 error = 0;
   if ( 0 > pConnection->m_fd )
   {
      error = pConnection->m_fd;
   }
   return error;
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
SwitchMgmtConnection*
SwitchMgmtPort_listen( SwitchMgmtConnection* pListeningConnection )
{
   // printf("SwitchMgmtPort listen\n" );
	SwitchMgmtConnection* pConnection = calloc( 1, sizeof(SwitchMgmtConnection_) );
	// accept awaiting request
	pConnection->m_fd = accept( pListeningConnection->m_fd,
			                    (struct sockaddr*)NULL ,NULL);
	sampleConnectionTime( pConnection );
	return pConnection;
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
uint32
SwitchMgmtPort_receive( SwitchMgmtConnection* pConnection,
                        void*                 pBuffer,
                        uint32                len        )
{
   uint32 res = read( pConnection->m_fd, pBuffer, len );
   // printf("SwitchMgmtPort received %d bytes:\n", res );
	//hexdump( pBuffer, res );
   sampleReceptionTime( pConnection );
	return res;
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
uint32
SwitchMgmtPort_send( SwitchMgmtConnection* pConnection,
                     const void*           pBuffer,
                     uint32                len        )
{
   calculateReceptionTime( pConnection );
   printf("SwitchMgmtPort send %d bytes:\n", len );
   //hexdump( pBuffer, len );
	return write( pConnection->m_fd, pBuffer, len );
}
