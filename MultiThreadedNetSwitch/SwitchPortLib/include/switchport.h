#ifndef SWITCH_PORT_H
#define SWITCH_PORT_H

#include <stdio.h>
#include <switchporttypes.h>

#if defined(__cplusplus)
extern "C"
{
#endif

typedef struct SwitchPort_           SwitchPort;

typedef enum
{
	ePORT_LINK_STATE_NOT_READY = 0,
	ePORT_LINK_STATE_LINKED,
	ePORT_LINK_STATE_DISABLED
} EnSasLinkState;

typedef enum
{
	ePORT_LINK_SPEED_SLOW = 0,
	ePORT_LINK_SPEED_NORMAL,
	ePORT_LINK_SPEED_FAST
} EnSasLinkSpeed;

///////////////////////////////////////////////////////////////////////////////
/// @brief  Construct a SasSwitchPort object.
/// @param  logicalPort   The logical port number.
/// @param  physicalPort  The physical port number.
/// @return A pointer to an allocated SwitchPort object
///////////////////////////////////////////////////////////////////////////////
SwitchPort*           SwitchPort_createSwitchPort( int logicalPort,
                                                   int physicalPort );
///////////////////////////////////////////////////////////////////////////////
/// @brief  Deletes a previous allocated SwitchPort object.
/// @param  pPort  A pointer to a previous allocated SwitchPort object.
/// @pre    pPort != 0
///////////////////////////////////////////////////////////////////////////////
void                  SwitchPort_delete( SwitchPort *pPort );

///////////////////////////////////////////////////////////////////////////////
/// @brief Getters
/// @{
///////////////////////////////////////////////////////////////////////////////
int                   SwitchPort_getLogicalPort ( const SwitchPort* pPort );
int                   SwitchPort_getPhysicalPort( const SwitchPort* pPort );
EnSasLinkSpeed        SwitchPort_getLinkSpeed   ( const SwitchPort* pPort );
EnSasLinkState        SwitchPort_getLinkState   ( const SwitchPort* pPort );
///////////////////////////////////////////////////////////////////////////////
/// @}
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
/// @brief Setters
/// @{
///////////////////////////////////////////////////////////////////////////////
void                  SwitchPort_setLinkSpeed( SwitchPort* pPort, EnSasLinkSpeed speed );
void                  SwitchPort_enable      ( SwitchPort* pPort );
void                  SwitchPort_disable     ( SwitchPort* pPort );
///////////////////////////////////////////////////////////////////////////////
/// @}
///////////////////////////////////////////////////////////////////////////////



#if defined(__cplusplus)
}
#endif

#endif // SWITCH_PORT_H
