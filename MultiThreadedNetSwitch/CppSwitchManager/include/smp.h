/*
 * smp.h
 *
 *  Created on: May 16, 2014
 *      Author: daniel
 */

#ifndef SMP_H_
#define SMP_H_

#include <map>
#include <list>
#include <memory>
#include <tuple>
// Move to SMP
#define CPU_LITTLE
#include "endianess.h"
#include "port.h"


namespace smp
{

   enum EnFrameType
   {
      eFrameTypeRequest  = 0x40,
      eFrameTypeResponse = 0x41
   };

   enum EnSmpFuntionCode
   {
      eSmpFuntionDiscoverList = 0x20,
      eSmpFuntionPhyControl   = 0x91
   };

   enum EnSmpFunctionResult
   {
      eSmpFunctionResultSmpFunctionAccepted = 0,
      eSmpFunctionResultUnknownSmpFunction,
      eSmpFunctionResultSmpFunctionFailed,
      eSmpFunctionResultInvalidRequestFrameLength,
      eSmpFunctionResultInvalidExpanderChangeCount
   };

   enum EnDescriptorType
   {
      eDescriptorTypeLongFormat,
      eDescriptorTypeShortFormat
   };

#pragma pack(1)
   struct SmpHeader
   {
      uint8 frameType;
      uint8 function;
   };

   struct SmpResponse
   {
      SmpHeader header;
      uint8     functionResult;
      uint8     responseLength;
   };

   struct SmpDiscoverListRequest
   {
      SmpHeader header;
      uint8     allocatedResponse;
      uint8     requestLength;
      uint32    reserved0;
      uint8     startingPhy;
      uint8     maxNumberOfDescriptors;
      DECLARE_3_BITFIELDS
      (
            ignoreZoneGroup, 1,
            reserved1      , 3,
            phyFilter      , 4
      );
      DECLARE_2_BITFIELDS
      (
            reserved2      , 4,
            descriptorType , 4
      );
   };

   struct SmpDiscoverDescriptor
   {
      uint8 logical;
      uint8 physical;
      uint8 speed;
      uint8 state;
   };

   struct SmpDiscoverListResponse
   {
      SmpResponse           response;
      uint8                 startingPhy;
      uint8                 maxNumberOfDescritprs;
      DECLARE_3_BITFIELDS
      (
            ignoreZoneGroup, 1,
            reserved1      , 3,
            phyFilter      , 4
      );
      DECLARE_2_BITFIELDS
      (
            reserved2      , 4,
            descriptorType , 4
      );
      SmpDiscoverDescriptor descritptorsPlaceHolder;
   };
#pragma pack()

   ////////////////////////////////////////////////////////////////////////////
   /// @brief  The SMP command handlers interface.
   ////////////////////////////////////////////////////////////////////////////
   struct SmpCommandHandler
   {
      virtual ~SmpCommandHandler() {}
      virtual uint8* processSmpRequest( const uint8* pBuffer, uint32& len ) = 0;

      static uint8* sendBadResponse( EnSmpFuntionCode fun, EnSmpFunctionResult res, uint32& len );
   };

   typedef std::shared_ptr< SmpCommandHandler > SmpCommandHandlerPtr;

   ////////////////////////////////////////////////////////////////////////////
   /// @brief  The SMP Discover list command processment
   ////////////////////////////////////////////////////////////////////////////
   struct SmpDiscoverListCommandHandler : public SmpCommandHandler
   {
      SmpDiscoverListCommandHandler( switchmanagement::SasPorts &ports );

      uint8* processSmpRequest( const uint8* pBuffer, uint32& len );

   private:
      switchmanagement::SasPorts m_ports;
   };

   ////////////////////////////////////////////////////////////////////////////
   /// @brief  The SMP Controller, invoked everytime there's a SMP command
   ///         to process.
   ////////////////////////////////////////////////////////////////////////////
   class SmpController
   {
   public:
      typedef std::map< EnSmpFuntionCode, SmpCommandHandlerPtr > SmpCommandHandlers;
      typedef SmpCommandHandlers::iterator                       SmpCommandHandlersIter;

      uint8* processSmpRequest( uint8* pBuffer, uint32& len );

      void AddHandler( EnSmpFuntionCode function, SmpCommandHandlerPtr pHandler );

      static SmpController& getInstance();
   private:
      // The constructor is private as this is a singleton.
      SmpController()
      {
      };

      // Hiding the copy constructor and assignment operator
      // to protect the singleton design pattern.
      SmpController( const SmpController&);
      SmpController& operator=( const SmpController& );

      SmpCommandHandlers m_handlers;

   };

}

#endif /* SMP_H_ */
