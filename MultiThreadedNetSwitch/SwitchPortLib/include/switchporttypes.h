/*
 * switchporttypes.h
 *
 *  Created on: May 11, 2014
 *      Author: daniel
 */

#ifndef SWITCHPORTTYPES_H_
#define SWITCHPORTTYPES_H_


typedef char           int8;
typedef unsigned char  uint8;
typedef unsigned short uint16;
typedef short          int16;
typedef int            int32;
typedef unsigned int   uint32;


#endif /* SWITCHPORTTYPES_H_ */
