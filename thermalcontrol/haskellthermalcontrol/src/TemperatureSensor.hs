{-# LANGUAGE ForeignFunctionInterface #-} 
module TemperatureSensor where

import Foreign.C -- get the C types
import Foreign

type Bus      = Int
type Address  = Int
data SensorId = SensorId Bus Address
   
data TemperatureSensor       = TemperatureSensor
type TemperatureSensorPtr    = Ptr TemperatureSensor
data TemperatureSensorHandle = TemperatureSensorHandle (ForeignPtr TemperatureSensor)

--
-- Import the temperature sensor lib function responsible for deleting temperature sensor as a
-- finalizer. Notice the '&' in the import statement and the argument type of 
-- 'FinalizerPtr'
-- 
foreign import ccall "temperaturesensor &TemperatureSensor_delete" c_TemperatureSensorDelete :: FinalizerPtr TemperatureSensor

--
-- Import the temperature sensor lib function responsible for creating a I2C temperature sensor
--
foreign import ccall "temperaturesensor TemperatureSensor_createI2cSensor" c_TemperatureSensorCreateI2cSensor :: CInt -> CInt -> IO TemperatureSensorPtr
createI2cTemperatureSensor :: SensorId -> IO ( TemperatureSensorHandle )
createI2cTemperatureSensor (SensorId bus address ) = do
   sensor <- c_TemperatureSensorCreateI2cSensor (fromIntegral bus) (fromIntegral address)
   sensorPtr <- newForeignPtr c_TemperatureSensorDelete sensor
   return (TemperatureSensorHandle  sensorPtr )


--
-- Import the temperature sensor lib function responsible for reading temperature sensor
-- 
foreign import ccall "temperaturesensor TemperatureSensor_read" c_TemperatureSensorRead :: Ptr TemperatureSensor -> IO CInt
readTemperatureSensor :: TemperatureSensorHandle -> IO Int
readTemperatureSensor (TemperatureSensorHandle  sensor ) = do
   withForeignPtr sensor $ \sensorPtr -> do
      temp <- c_TemperatureSensorRead sensorPtr
      return $ fromIntegral temp

   