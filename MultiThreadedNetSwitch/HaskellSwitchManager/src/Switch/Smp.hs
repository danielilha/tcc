-- |
-- Module      : $Header$
-- Description : This modules implements a simplified version of the SMP protocol
--               handler. The intent is just to verify how Haskell performs
--               when parsing binary data (bytes and bitfields). 
--               
-- Copyright   : (c) daniel.ilha@gmail.com
-- Maintainer  : daniel.ilha@gmail.com
-- Stability   : experimental
-- Portability : portable
-- 
module Switch.Smp where

import Switch.Port                         as Port
import qualified Data.ByteString           as B
import qualified Data.ByteString.Lazy      as BL
import qualified Data.Tuple                as T
import qualified Data.Maybe                as M
import qualified Data.Binary.Strict.Get    as G
import qualified Data.Binary.Put           as P
import qualified Data.Binary.Strict.BitGet as BG
import Data.Word
import Control.Applicative

-- |
-- DiscoverList PHY filter type enum
-- Haskell enums are Data types sequentially associated to a number.
-- They are translatable to Strings as any other data type that 
-- derive from Show.
--
data PhyFilter       = AllPhys | AttachedToExpander | AttachedToAnything deriving (Enum, Show)

-- |
-- DiscoverList descriptor type enum
--
data DescritptorType = LongFormat | ShortFormat deriving (Enum, Show)

-- |
-- SMP funcion codes enum. Just a small subset of all the commands supported thru SMP
--
data SmpFunctionCode = DiscoverList | PhyControl | Unknown deriving (Eq, Show)
instance Enum SmpFunctionCode where
   fromEnum = M.fromJust . flip lookup table
   toEnum   = M.fromJust . flip lookup (Prelude.map T.swap table)
table = [(DiscoverList, 0x20), (PhyControl, 0x91), (Unknown, 0xff)]  

-- |
-- SMP function results enum.
--
data SmpFunctionResult = SmpFunctionAccepted | UnknownSmpFunction | SmpFunctionFailed | InvalidRequestFrameLength | InvalidExpanderChangeCount deriving (Eq, Show, Enum)

-- |
-- SMP frames.
-- 
data SmpFrame =  SmpDiscoverListRequest 
                  { allocatedResponseLength :: Word8 
                  , requestLength           :: Word8
                  , startingPhy             :: Word8
                  , maxNumberOfDescriptors  :: Word8
                  , ignoreZoneGroup         :: Bool            -- ^ 1 bit
                  , phyFilter               :: PhyFilter       -- ^ 4 bits
                  , descriptorType          :: DescritptorType -- ^ 4 bits
                  }
                | UnknownRequest
                deriving (Show)


-- |
-- SMP discover descriptor.
-- One descriptor like this is returned for each phy
data SmpDiscoverDescriptor = SmpDiscoverListShortFormatDescriptor
                              { logical           :: Word8
                              , physical          :: Word8
                              , speed             :: Word8           -- ^ 4 bits
                              , state             :: Word8           -- ^ 4 bits
                              }
                              deriving (Show)

-- | 
-- SMP Discover List parser
-- 
decodeDiscoverList :: G.Get SmpFrame
decodeDiscoverList = do
   allocRespLen <- G.getWord8
   requestLen   <- G.getWord8
   G.skip 4
   startPhy     <- G.getWord8
   maxDesc      <- G.getWord8
   byte10Flags  <- G.getByteString 1
   byte11Flags  <- G.getByteString 1
   
   let r = BG.runBitGet byte10Flags $ do 
         ignoreZoneGrp   <- BG.getBit
         BG.skip 3
         phyFilt         <- BG.getAsWord8 4 
      
         let r = BG.runBitGet byte11Flags $ do
               BG.skip 4
               descriptorT    <- BG.getAsWord8 4 
         
               return $ SmpDiscoverListRequest allocRespLen requestLen startPhy maxDesc ignoreZoneGrp (toEnum (fromIntegral phyFilt :: Int) ) ( toEnum (fromIntegral descriptorT :: Int)) 
      
         case r of
            Left error -> fail error
            Right x -> return x 
   
   case r of
      Left error -> fail error
      Right x -> return x

decodeRequest :: G.Get SmpFrame
decodeRequest = do
   G.skip 1
   function  <- G.getWord8
   case toEnum (fromIntegral function :: Int) of
      DiscoverList -> decodeDiscoverList
      _            -> return UnknownRequest

-- |
-- Encode a SMP response. It can be used toghether with other encoding
-- functions.
--    
encodeResponse :: SmpFunctionCode -> SmpFunctionResult -> Word8 -> P.Put
encodeResponse fun res len = do 
   P.putWord8 0x41
   P.putWord8 (fromIntegral (fromEnum fun) :: Word8 )
   P.putWord8 (fromIntegral (fromEnum res) :: Word8 )
   P.putWord8 len

-- |
-- Handles SMP requests and responses. It receives and return a bytestring.
--
handleManagementProtocol :: [Port.SwitchPortHandle] -> B.ByteString -> IO B.ByteString
handleManagementProtocol ports input = do
   let res = G.runGet decodeRequest input
   case fst res of
      Right req@SmpDiscoverListRequest {} -> BL.toStrict <$>  P.runPut <$> encodeDiscoverList req <$> mapM getAsDescriptor ports   
      _                                   -> return $ BL.toStrict ( P.runPut (  encodeResponse DiscoverList SmpFunctionFailed 0  ) )
    
-- |
-- Create the SMP discover descriptor for a port.
-- 
getAsDescriptor :: Port.SwitchPortHandle -> IO SmpDiscoverDescriptor
getAsDescriptor port =  SmpDiscoverListShortFormatDescriptor <$> ( fromIntegral <$> Port.getLogicalPort  port )
                                                             <*> ( fromIntegral <$> Port.getPhysicalPort port )
                                                             <*> ( fromIntegral <$> ( fromEnum <$> Port.getLinkSpeed port  ))
                                                             <*> ( fromIntegral <$> ( fromEnum <$> Port.getLinkState port )) 

-- |
-- Encode a complete discover list response
--
encodeDiscoverList:: SmpFrame -> [SmpDiscoverDescriptor] -> P.Put
encodeDiscoverList request ports@(x:_) = do 
   encodeResponse DiscoverList SmpFunctionAccepted 0 
   P.putWord8 $ logical x
   P.putWord8 $ maxNumberOfDescriptors request
   encodeDiscoverListDescriptors ports
    
   
-- |
-- Encode the descriptors part of the discover list command response.
-- 
encodeDiscoverListDescriptors :: [SmpDiscoverDescriptor] -> P.Put
encodeDiscoverListDescriptors [] = return() 
encodeDiscoverListDescriptors ( SmpDiscoverListShortFormatDescriptor log phy sp st : xs) = do
   P.putWord8 log
   P.putWord8 phy
   P.putWord8 ( fromIntegral (fromEnum sp  ) :: Word8 )
   P.putWord8 ( fromIntegral (fromEnum st  ) :: Word8 )
   encodeDiscoverListDescriptors xs
   
