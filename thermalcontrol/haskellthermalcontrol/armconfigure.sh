#!/bin/sh

cabal configure -w arm-unknown-linux-gnueabihf-ghc --with-hc-pkg=arm-unknown-linux-gnueabihf-ghc-pkg --with-ld=arm-linux-gnueabihf-ld --extra-lib-dirs=/usr/local/arm-linux/lib/ --extra-include-dirs=/usr/local/arm-linux/include/ $@
