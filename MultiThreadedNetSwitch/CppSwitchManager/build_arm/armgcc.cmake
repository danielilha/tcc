SET(CMAKE_SYSTEM_NAME Linux)  # Tell CMake we're cross-compiling
set(CMAKE_SYSTEM_PROCESSOR arm)
include(CMakeForceCompiler)
# Prefix detection only works with compiler id "GNU"
# CMake will look for prefixed g++, cpp, ld, etc. automatically
CMAKE_FORCE_C_COMPILER(arm-linux-gnueabihf-gcc GNU)
CMAKE_FORCE_CXX_COMPILER(arm-linux-gnueabihf-g++ GNU)

# where is the target environment 
SET (CMAKE_FIND_ROOT_PATH  /usr/local/arm-linux/)

set (CMAKE_C_COMPILER   arm-linux-gnueabihf-gcc )
set (CMAKE_CXX_COMPILER arm-linux-gnueabihf-g++ )

# search for programs in the build host directories
SET(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
# for libraries and headers in the target directories
SET(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
SET(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
