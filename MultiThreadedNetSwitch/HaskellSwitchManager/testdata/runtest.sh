#!/bin/bash

ITERS=$1
SLEEP=$2
PORT=$3

if [[ "$PORT" == "" ]]
then
   PORT=5002
fi

while [[ $ITERS -gt 0 ]]
do 
   ITERS=`expr $ITERS - 1`
   cat discover | nc localhost $PORT | hexdump -C; 
   sleep $SLEEP
done
