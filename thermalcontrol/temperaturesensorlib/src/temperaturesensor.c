#include <stdlib.h>
#include <assert.h>
#include <syslog.h>
#include "temperaturesensor.h"

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
typedef int  ( *ReadTemperatureCb )( TemperatureSensor* pSensor );
typedef void ( *DeleteSensorCb    )( TemperatureSensor* pSensor );

///////////////////////////////////////////////////////////////////////////////
/// @brief A base 'class' for temperature sensors
///////////////////////////////////////////////////////////////////////////////
typedef struct TemperatureSensor_
{
	ReadTemperatureCb m_readCb;
	DeleteSensorCb    m_deleteCb;
} TemperatureSensor_;

///////////////////////////////////////////////////////////////////////////////
/// @brief Extending the base TemperatureSensor with I2C specific data.
///////////////////////////////////////////////////////////////////////////////
typedef struct I2cTemperatureSensor
{
	TemperatureSensor_ m_base;
	int                m_bus;
	int                m_address;
	int                m_numOfFailures;
} I2cTemperatureSensor;

///////////////////////////////////////////////////////////////////////////////
/// @brief Private functions
///////////////////////////////////////////////////////////////////////////////
static void               I2cTemperatureSensor_delete         ( TemperatureSensor *pSensor );
static int                I2cTemperatureSensor_read           ( TemperatureSensor *pSensor );

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
TemperatureSensor* TemperatureSensor_createI2cSensor( int bus, int address )
{
	I2cTemperatureSensor * pSensor = malloc( sizeof(I2cTemperatureSensor) );
	pSensor->m_base.m_deleteCb = &I2cTemperatureSensor_delete;
	pSensor->m_base.m_readCb   = &I2cTemperatureSensor_read;
	pSensor->m_bus             = bus;
	pSensor->m_address         = address;
	openlog("tempsensor", LOG_CONS, LOG_USER );
	syslog ( LOG_DEBUG, "creating 0x%x, 0x%x", pSensor->m_bus, pSensor->m_address );

	return (TemperatureSensor*)pSensor;
}
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
void               TemperatureSensor_delete         ( TemperatureSensor *pSensor )
{
	assert ( 0 != pSensor );
	pSensor->m_deleteCb( pSensor );
}
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
int                TemperatureSensor_read           ( TemperatureSensor *pSensor )
{
	assert ( 0 != pSensor );
	return pSensor->m_readCb( pSensor );
}

///////////////////////////////////////////////////////////////////////////////
/// @brief Function responsible for freeing up the resources allocated
///        for a I2C temperature sensor.
///////////////////////////////////////////////////////////////////////////////
void               I2cTemperatureSensor_delete         ( TemperatureSensor *pSensor )
{
	I2cTemperatureSensor* pI2cSensor = (I2cTemperatureSensor*) pSensor;
	syslog ( LOG_DEBUG, "deleting 0x%x, 0x%x", pI2cSensor->m_bus, pI2cSensor->m_address );
	closelog();
	free( pSensor );
}
///////////////////////////////////////////////////////////////////////////////
/// @brief Function responsible for reading temperature from an I2C sensor.
/// @note  This is currently returning a random value for testing purpose.
///////////////////////////////////////////////////////////////////////////////
int                I2cTemperatureSensor_read           ( TemperatureSensor *pSensor )
{
	I2cTemperatureSensor* pI2cSensor = (I2cTemperatureSensor*) pSensor;
	int temperature = random() % 100;
	syslog ( LOG_DEBUG, "reading 0x%x, 0x%x: %d C", pI2cSensor->m_bus, pI2cSensor->m_address, temperature );
	return temperature;
}
