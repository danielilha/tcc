/*
 * smp.cpp
 *
 *  Created on: May 16, 2014
 *      Author: daniel
 */



#include "smp.h"

using namespace smp;


uint8* SmpCommandHandler::sendBadResponse( EnSmpFuntionCode fun, EnSmpFunctionResult res, uint32& len )
{
   SmpResponse* pResponse =  new SmpResponse;
   pResponse->header.frameType = eFrameTypeResponse;
   pResponse->header.function  = fun;
   pResponse->functionResult   = res;
   len = sizeof(SmpResponse);
   return reinterpret_cast<uint8*>( pResponse );
}

SmpDiscoverListCommandHandler::SmpDiscoverListCommandHandler( switchmanagement::SasPorts &ports )
: m_ports( ports )
{

}

uint8* SmpDiscoverListCommandHandler::processSmpRequest( const uint8* pBuffer, uint32& len )
{
   uint8* pResponse;
   // This kind of check is done internally by the Haskell Binary.Get monad
   // and it's error report is really intuitive, btw, it doens't crash.
   if ( len >= sizeof( SmpDiscoverListRequest ) )
   {
      const SmpDiscoverListRequest* pRequest = reinterpret_cast< const SmpDiscoverListRequest* > ( pBuffer );
      len                      = sizeof( SmpDiscoverListResponse ) + (sizeof( SmpDiscoverDescriptor ) * ( m_ports.size() -1 ) );
      const uint32 responseLen = len - sizeof( SmpDiscoverListResponse ) + sizeof( SmpDiscoverDescriptor );

      pResponse = (uint8*)calloc( 1, len );
      SmpDiscoverListResponse* pDiscoverResponse = reinterpret_cast< SmpDiscoverListResponse* >( pResponse );

      pDiscoverResponse->response.header.frameType = eFrameTypeResponse;
      pDiscoverResponse->response.header.function  = eSmpFuntionDiscoverList;
      pDiscoverResponse->response.functionResult = eSmpFunctionResultSmpFunctionAccepted;
      pDiscoverResponse->response.responseLength = responseLen;
      pDiscoverResponse->descriptorType          = pRequest->descriptorType;
      pDiscoverResponse->startingPhy             = pRequest->startingPhy;
      pDiscoverResponse->maxNumberOfDescritprs   = pRequest->maxNumberOfDescriptors;

      SmpDiscoverDescriptor* pDescriptor = &pDiscoverResponse->descritptorsPlaceHolder;
      for ( switchmanagement::SasPortsIter iter = m_ports.begin() ;
            m_ports.end() != iter;
            iter ++)
      {
         pDescriptor->logical = (*iter)->getLogicalPort();
         pDescriptor->physical= (*iter)->getPhysicalPort();
         pDescriptor->speed   = (*iter)->getLinkSpeed();
         pDescriptor->state   = (*iter)->getLinkState();
         pDescriptor++;
      }
   }
   else
   {
      pResponse = sendBadResponse( eSmpFuntionDiscoverList, eSmpFunctionResultInvalidRequestFrameLength, len );
   }
   return pResponse;
}


uint8* SmpController::processSmpRequest( uint8* pBuffer, uint32& len )
{
   uint8* pResponse;
   if ( len > sizeof(SmpHeader) )
   {
      SmpHeader * pHeader = reinterpret_cast< SmpHeader * >( pBuffer );
      SmpCommandHandlersIter iter = m_handlers.find( (EnSmpFuntionCode) pHeader->function );
      if ( m_handlers.end() != iter )
      {
         pResponse = (*iter).second->processSmpRequest( pBuffer, len );
      }
      else
      {
         pResponse = SmpCommandHandler::sendBadResponse( (EnSmpFuntionCode)0, eSmpFunctionResultUnknownSmpFunction, len );
      }
   }
   else
   {
      pResponse = SmpCommandHandler::sendBadResponse( (EnSmpFuntionCode)0, eSmpFunctionResultInvalidRequestFrameLength, len );
   }
   return pResponse;
}

void SmpController::AddHandler( EnSmpFuntionCode function, SmpCommandHandlerPtr pHandler )
{
   m_handlers[ function ] = pHandler;
}

SmpController& SmpController::getInstance()
{
   static SmpController* s_pMyInstance = new SmpController();

   return *s_pMyInstance;
}



