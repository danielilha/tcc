module Main where

import ThermalControl
import Control.Concurrent()
import qualified TemperatureSensor
   

main::IO()
main = do
   let defaultCurves = ([ (10, 20), (14, 25), (20, 40), (50, 60), (75, 80), (90, 90) ], 
                        [ ( 8, 20), (12, 25), (18, 40), (48, 60), (73, 80), (88, 90) ] )

   sensors <- mapM TemperatureSensor.createI2cTemperatureSensor [ (TemperatureSensor.SensorId 0 0xa0), (TemperatureSensor.SensorId 0 0xa2), (TemperatureSensor.SensorId 0 0xa4) ]
   continuosThermalControl sensors
                           TemperatureSensor.readTemperatureSensor          
                           discreteThermalAlgorithm 
                           [ DiscreteThermalAlgorithmData{ tempVsSpeedCurves=defaultCurves, currentCoolingPower=0, previousTemperature = 0} | _<- [1..] ]

                            
    
